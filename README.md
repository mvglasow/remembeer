This is a resurrection of the Remembeer app on F-Droid, which appears to be unmaintained since 2011.

Some bugs were fixed, and there were some upgrades behind the scenes:

* Ensure the list is updated after adding a new beer
* Do not use title case other than in the beer name
* Use full-screen activities instead of dialogs
* Restore compatibility with current Android (and toolchain) versions
* Switch build to gradle
* Replace in-tree binary blobs with versions from repositories
* Move F-Droid metadata into the source tree
* Add GitLab CI with F-Droid, and generate an APK signed with a debug key

**This repository is up for grabs.** I am not planning on making any major changes, my intention is just to keep this app usable for myself and let others benefit from that. If you would like to maintain it, please let me know (e.g. by opening an issue).
